$(document).ready(function(){
    $(".collapse.show").each(function(){
      $(this).prev(".card-header").find(".fa").addClass("fa-angle-down").removeClass("fa-angle-up");
    });
    
    // Toggle right and down arrow icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function(){
      $(this).prev(".card-header").find(".fa").removeClass("fa-angle-up").addClass("fa-angle-down");
    }).on('hide.bs.collapse', function(){
      $(this).prev(".card-header").find(".fa").removeClass("fa-angle-down").addClass("fa-angle-up");
    });
})

$("#search-input").keyup(function(event) {
  if (event.keyCode === 13) {
      $("#search-btn").click();
  }
});