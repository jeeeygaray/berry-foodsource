function openNav() {
    document.getElementById("mySidepanel").classList.add('side-width');
    document.getElementById("mySidepanel").classList.remove('side-width-none');
    document.getElementById("overlay").style.display = "block";
  }

  function closeNav() {
    document.getElementById("mySidepanel").classList.remove('side-width');
    document.getElementById("mySidepanel").classList.add('side-width-none');
    document.getElementById("overlay").style.display = "none";
  }